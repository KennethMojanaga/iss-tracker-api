require('dotenv').config();
require('./util/logger');
const constants = require('./constants');
const fs = require('fs');

const express = require('express');

// Initialize mongodb connection
const mongodbManager = require('./io/mongodb/mongodb');
mongodbManager.initInstance();

// Business Logic
const locationManager = require('./api/location_manager');

// Apollo-Server Imports
const { ApolloServer, gql } = require('apollo-server-express');
const { createServer } = require('http');
const { execute, subscribe } = require('graphql');
const { SubscriptionServer } = require('subscriptions-transport-ws');
const { makeExecutableSchema } = require('@graphql-tools/schema');
const {
    SyntaxError,
    ApolloError,
    ForbiddenError,
    UserInputError,
    ValidationError,
    AuthenticationError,
} = require('apollo-server-express');

const { PubSub } = require('graphql-subscriptions');
const pubsub = new PubSub();


// GQL schema and resolvers
const GQL_SCHEMA_FILE = "./graphql/schema.graphql";
const typeDefs = gql(fs.readFileSync(GQL_SCHEMA_FILE, { encoding: 'utf-8' }));
const resolvers = require('./graphql/resolvers/resolver');

(async () => {
    const app = express();
    const httpServer = createServer(app);
    
    const schema = makeExecutableSchema({ typeDefs, resolvers });

    const apolloServer = new ApolloServer({
        schema,
        plugins: [
            {
                async serverWillStart() {
                    return {
                        async drainServer() {
                            subscriptionServer.close();
                        }
                    };
                }
            }
        ],
        formatError: (err) => {
            if (err.message === null || err.message === undefined) {
                err.message = 'Server connection error, please try again or contact support.';
            }

            err.errorType = err.extensions.errorType;
            err.errorInfo = err.extensions.errorInfo;
            
            console.log('Error Message: ' + err.originalError.message);
            console.log('Error Stack: ' + err.originalError.stack);
            return err;
        },
        context: ({ req, res }) => {
            return {
                request: req,
                response: res,
                pubsub: pubsub,
                error_types: {
                    SyntaxError,
                    ApolloError,
                    ForbiddenError,
                    UserInputError,
                    ValidationError,
                    AuthenticationError,
                },
                
            };
        },
        debug: false,
        introspection: process.env.GQL_INTROSPECT === 'true',
        playground: process.env.GQL_PLAYGROUND === 'true',
    });

    const subscriptionServer = SubscriptionServer.create(
        {
            schema,
            execute,
            subscribe,
            onConnect(connectionParams, webSocket, context) {
                console.log('Connected!')
                // TODO: Secure the app
                // if (connectionParams.authorization) {
                //     const currentUser = await findUser(connectionParams.authorization);
                //     return { currentUser };
                // }
                // throw new Error('Missing auth token!');
                context.pubsub = pubsub
                return context;

            },
            onDisconnect(webSocket, context) {
                console.log('Disconnected!')
            },
        },
        {
            server: httpServer,
            path: apolloServer.graphqlPath,
        }
    );

    await apolloServer.start();
    apolloServer.applyMiddleware({ app, path: '/graphql' });

    // Run iss-location updater in background
    locationUpdateBackgroundRunner(pubsub);

    httpServer.listen(5050, () => console.log('🚀 Server Ready and Running'));
})();


function locationUpdateBackgroundRunner(pubSub=new PubSub()) {
    setInterval(async ()=> {
        const newIssLocation = await locationManager.updateISSLocation();

        const locationTrail = await locationManager.getISSLocationTrail();
        newIssLocation.location_trail = locationTrail;

        pubSub.publish(constants.LOCATION_UPDATED, {
            locationUpdated: newIssLocation
        });
    }, 5000);
}