const fetch = require("node-fetch");
const ObjectID = require("bson-objectid");

/**
 * This function retrieves the current location of the International Space Station
 * @returns {{
 *  id: ObjectID,
 *  datetime: Date,
 *  latitude: number,
 *  longitude: Number,
 *  location_trail: [Object]
 * }}
 */
const getCurrentISSLocation = async () => {
    const res = await fetch('http://api.open-notify.org/iss-now.json');
    const data = await res.json();

    const id = ObjectID();
    
    // First convert the timestamp from UTC-Epoch to date
    const date = new Date(0);
    date.setUTCSeconds(data.timestamp);

    const returnObj = {
        id: id,
        datetime: date,
        latitude: parseFloat(data.iss_position.latitude),
        longitude: parseFloat(data.iss_position.longitude),
        location_trail: []
    };

    return returnObj;
}

module.exports = {
    getCurrentISSLocation
};