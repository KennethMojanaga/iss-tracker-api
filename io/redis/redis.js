const redis = require('redis');
const CONSTANTS = require('../../constants');

const RedisClientFactory = (function(){
    let instance = null;
    return {
        /**
         * Get redis client singleton
         * @returns {redis.RedisClient}
         */
        getInstance: function(){
            if (instance == null) {
                instance = redis.createClient({
                    host: CONSTANTS.REDIS.HOST,
                    port: CONSTANTS.REDIS.PORT,
                });

                instance.on('error', err => {
                    console.log('Redis Error ' + err);
                });
            }
            return instance;
        }
   };
})();

module.exports = RedisClientFactory;