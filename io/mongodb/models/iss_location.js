const mongoose = require('mongoose');
const mongooseLeanDefaults = require('mongoose-lean-defaults').default;

const ISSLocationSchema = new mongoose.Schema({
    datetime: { type: Date, required: true },
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true },

});

// Add compound index
ISSLocationSchema.index({ datetime: 1 }, { unique: true });

// Add plugin
ISSLocationSchema.plugin(mongooseLeanDefaults);

module.exports = mongoose.model('ISSLocation', ISSLocationSchema);