const mongoose = require('mongoose');
const CONSTANTS = require('../../constants');
const { getInstance } = require('../redis/redis');

const MongoDBConnFactory = (function(){
    let connInstance = null;
    return {
        /**
         * Initialize and maintain single db connection
         * @returns {mongoose.Connection}
         */
        initInstance: function(){
            if (connInstance == null) {
                mongoose.connect(CONSTANTS.MONGO_DB.URL, {
                    useNewUrlParser: true,
                    // useUnifiedTopology: true, poolSize: 5 // 5 operations can run at a time
                });

                connInstance = mongoose.connection;

                connInstance.on('error', function(error) {
                    console.log('MongoDB connection error: ', error)
                });
                connInstance.once('open', async function callback () {
                    console.log("Mongoose connected successfully");
                });
            }
        },
   };
})();

module.exports = MongoDBConnFactory;