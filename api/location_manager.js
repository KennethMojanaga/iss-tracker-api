// I/O imports
const locationApiAdapter = require('../io/http/open_notify');
const redisClientFactory = require('../io/redis/redis');
const ISSLocationModel = require('../io/mongodb/models/iss_location');

const async = require('async');


const mongodbManager = require('../io/mongodb/mongodb');
mongodbManager.initInstance();

const updateISSLocation = async () => {
    const MAX_CACHE_LIFE = 60 * 60; // 1 hour

    // retreive current iss location
    const locationData = await locationApiAdapter.getCurrentISSLocation();

    // Persist to the database
    const createdDBData = await ISSLocationModel.create(locationData);
    if (!createdDBData) {
        console.log('An error occured whilst attempting to persist location to the db');
    }

    // Persist to cache
    const redisClient = redisClientFactory.getInstance();

    const redisPayload = JSON.stringify(locationData);
    redisClient.setex(locationData.datetime, MAX_CACHE_LIFE, redisPayload);

    return locationData;
}

const getISSLocationTrail = async () => {
    // First check the cache
    const redisClient = redisClientFactory.getInstance();

    return new Promise(function (resolve, reject) {
       // Get all keys
        redisClient.keys('*', async function (err, keys) {
            if (err) return console.log(err);

            if (keys) {
                async.map(keys, function(key, cb) {
                    redisClient.get(key, function (err, value) {
                        if (err) {
                            console.log(err);
                            reject(err);
                        }

                        const valObj = JSON.parse(value);
                        cb(null, valObj);
                    });

                }, function (error, results) {
                    if (error) {
                        console.log(error);
                        reject(error);
                    }
                    
                    resolve(results);
                });
            } else {
                // There's nothing in the cache, so go to the db
                const now = new Date();

                // NB: The application is persisting data every 5 seconds
                // This means that for every hour, there should be 720 data points created
                const RECORDS_PER_HOUR = 720;

                try {
                    const lastHourData = await ISSLocationModel.find({
                                                        datetime: { $lte: now }
                                                    })
                                                    .limit(RECORDS_PER_HOUR)
                                                    .lean({ defaults: true })
                                                    .sort({ datetime: 1 });

                    resolve(lastHourData);
                } catch (error) {
                    console.log(error);
                }
            }
        }); 
    });
}

module.exports = {
    updateISSLocation,
    getISSLocationTrail
}