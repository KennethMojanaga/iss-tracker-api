const fetch = require("node-fetch");
const constants = require("../../constants");

const LOCATION_UPDATED = constants.LOCATION_UPDATED;

const resolvers = {
    // Query: {
    //     issLocations: async (parent, args, context, info) => {
    //         const startDate = args.startDate;
    //         const endDate = args.endDate;
    //         return testData;
    //     }
    // },
    // Mutation: {
    //     updateISSLocation: async (parent, args, context, info) => {
    //         const pubsub = context.pubsub;
    //         const res = await fetch('http://api.open-notify.org/iss-now.json');
    //         const data = await res.json();
            
    //         const publishPayload = {
    //             id: data.timestamp,
    //             datetime: data.timestamp,
    //             latitude: data.iss_position.latitude,
    //             longitude: data.iss_position.longitude
    //         };

    //         pubsub.publish(LOCATION_UPDATED, {
    //             locationUpdated: publishPayload
    //         });

    //         return publishPayload;
    //     }
    // },
    Subscription: {
        locationUpdated: {
            subscribe: async (parent, args, context, info) => {
                const pubSub = context.pubsub;
                return pubSub.asyncIterator(LOCATION_UPDATED);
            }
        }
    }
}

module.exports = resolvers