const LOCATION_UPDATED = "LOCATION_UPDATED";

const REDIS = {
    HOST: process.env.REDIS_HOST,
    PORT: process.env.REDIS_PORT,
};

const MONGO_DB = {
    URL: process.env.MONGO_URL
};

module.exports = {
    LOCATION_UPDATED,
    REDIS,
    MONGO_DB
};